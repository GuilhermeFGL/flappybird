package com.guilhermefgl.flappybird;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.VideoView;

public class SplashActivity extends Activity implements MediaPlayer.OnCompletionListener {

    VideoView introVV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        introVV = (VideoView) this.findViewById(R.id.surface);
        introVV.setVideoURI(
                Uri.parse("android.resource://"+ getPackageName() +"/raw/" + R.raw.intro));
        introVV.setOnCompletionListener(this);
        introVV.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                startGame();
                return false;
            }
        });
        introVV.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        startGame();
    }

    private void startGame() {
        startActivity(new Intent(this, AndroidLauncher.class));
        finish();
    }
}
