package com.guilhermefgl.flappybird.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class SharedPreferences {

    public static void saveHighScore(int score) {
        if (score > getHighScore()) {
            Preferences prefs = Gdx.app.getPreferences(Config.PREFERENCES_NAME);
            prefs.putInteger(Config.PREFERENCES_SCORE, score);
            prefs.flush();
        }
    }

    public static int getHighScore() {
        Preferences prefs = Gdx.app.getPreferences(Config.PREFERENCES_NAME);
        int score = prefs.getInteger(Config.PREFERENCES_SCORE, 0);
        return score < 0 ? 0: score;
    }
}
