package com.guilhermefgl.flappybird.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;

public final class Config {

    public static final String PREFERENCES_NAME = "FlappyBird";
    public static final String PREFERENCES_SCORE = "score";

    public static final float VIRTUAL_WIDTH = 768;
    public static final float VIRTUAL_HEIGHT = 1024;

    public enum GAME_STATE {
        BEGINNING,
        RUNNING,
        ENDGAME
    }

    public static final Color FONT1_COLOR = Color.WHITE;
    public static final int FONT1_SIZE = 6;
    public static final Color FONT2_COLOR = Color.WHITE;
    public static final int FONT2_SIZE = 3;
    public static final Color FONT3_COLOR = Color.WHITE;
    public static final int FONT3_SIZE = 2;

    public static final Texture[] PERSON_TX = {
            new Texture("passaro1.png"),
            new Texture("passaro2.png"),
            new Texture("passaro3.png")
    };
    public static final int PERSON_RATIO = 120;
    public static final Texture BACKGROUND_TX = new Texture("fundo.png");
    public static final Texture OBSTACLE_UP_TX = new Texture("cano_topo.png");
    public static final Texture OBSTACLE_DOWN_TX = new Texture("cano_baixo.png");
    public static final Texture GAME_OVER_TX = new Texture("game_over.png");
    public static final int OBSTACLE_WIDTH = 120;
    public static final int OBSTACLE_HEIGHT = 400;

    public static final String RESTART_TEXT = "Toque para reiniciar!";
    public static final String HIGH_SCORE_TEXT = "maior pontuação: ";
    public static final int TITLE_RATIO = 230;
    public static final int RESTART_RATIO = 200;
    public static final int HIGH_SCORE_RATIO = 50;

    public static float jumpRange = 15;
    public static float foregroundMovement = 10;
    public static float backgroundMovement = 1L;
    public static float spaceRange = 300;
    public static float obstacleMovement = 200;
    public static int difficultyDivider = 10;
    private static float originalJumpRange = 15;
    private static float originalForegroundMovement = 10;
    private static float originalBackgroundMovement = 1L;
    private static float originalSpaceRange = 300;
    private static float originalObstacleMovement = 200;
    private static float incrementForegroundMovement = 2;
    private static float incrementBackgroundMovement = 1/2;
    private static float decrementSpaceRange = 10;
    private static float incrementObstacleMovement = 50;
    private static float limitForegroundMovement = 30;
    private static float limitBackgroundMovement = 10;
    private static float limitSpaceRange = 200;
    private static float limitObstacleMovement = 1000;

    public static void difficulty() {
        if (foregroundMovement + incrementForegroundMovement < limitForegroundMovement) {
            foregroundMovement += incrementForegroundMovement;
        }

        if (backgroundMovement + incrementBackgroundMovement < limitBackgroundMovement) {
            backgroundMovement += incrementBackgroundMovement;
        }

        if (spaceRange - decrementSpaceRange > limitSpaceRange) {
            spaceRange -= decrementSpaceRange;
        }

        if (obstacleMovement + incrementObstacleMovement < limitObstacleMovement) {
            obstacleMovement += incrementObstacleMovement;
        }
    }

    public static void restart() {
        jumpRange = originalJumpRange;
        foregroundMovement = originalForegroundMovement;
        backgroundMovement = originalBackgroundMovement;
        spaceRange = originalSpaceRange;
        obstacleMovement = originalObstacleMovement;
    }


    // TODO
    /*
     * HIGH SCORE
     */

    /*
     * STRING RESOURCES
     */

}
