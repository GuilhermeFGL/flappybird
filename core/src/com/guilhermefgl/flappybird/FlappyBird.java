package com.guilhermefgl.flappybird;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.guilhermefgl.flappybird.util.Config;
import com.guilhermefgl.flappybird.util.ParallaxBackground;
import com.guilhermefgl.flappybird.util.ParallaxLayer;
import com.guilhermefgl.flappybird.util.SharedPreferences;

import java.util.Random;

class FlappyBird extends ApplicationAdapter {

    private SpriteBatch batch;
    private Random randomNumber;
    private BitmapFont font;
    private BitmapFont message;
    private BitmapFont highscore;
    private Circle personArea;
    private ParallaxBackground background;
    private OrthographicCamera camera;
    private Viewport viewport;

    private float deviceWidth;
    private float deviceHeight;
    private int score = 0;
    private int lastScore = score;
    private Config.GAME_STATE gameState = Config.GAME_STATE.BEGINNING;

    private float localRange = 0;
    private float fallingRange = 0;
    private float verticalInitialPosition;
    private float horizontalObstacleMovementPosition;
    private float randomSpaceRange;
    private boolean getScore = false;

    @Override
    public void create () {
        batch = new SpriteBatch();
        randomNumber = new Random();
        personArea = new Circle();
        font = new BitmapFont();
        font.setColor(Config.FONT1_COLOR);
        font.getData().setScale(Config.FONT1_SIZE);

        message = new BitmapFont();
        message.setColor(Config.FONT2_COLOR);
        message.getData().setScale(Config.FONT2_SIZE);

        highscore = new BitmapFont();
        highscore.setColor(Config.FONT3_COLOR);
        highscore.getData().setScale(Config.FONT3_SIZE);

        camera = new OrthographicCamera();
        camera.position.set(Config.VIRTUAL_WIDTH / 2,
                Config.VIRTUAL_HEIGHT / 2, 0);
        viewport = new StretchViewport(Config.VIRTUAL_WIDTH, Config.VIRTUAL_HEIGHT, camera);

        deviceWidth = Config.VIRTUAL_WIDTH;
        deviceHeight = Config.VIRTUAL_HEIGHT;

        verticalInitialPosition = deviceHeight / 2;
        horizontalObstacleMovementPosition = deviceWidth;

        background = new ParallaxBackground(new ParallaxLayer[]{
                new ParallaxLayer(
                        new TextureRegion(Config.BACKGROUND_TX, 0, 0, Config.BACKGROUND_TX.getWidth(), Config.BACKGROUND_TX.getHeight()),
                        new Vector2(1, 1),
                        new Vector2(0, 0)),
        }, Config.VIRTUAL_WIDTH, Config.VIRTUAL_HEIGHT, new Vector2(50, 0));

    }

    @Override
    public void render () {
        camera.update();

        // Limpar frames anteriores
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        float deltaTime = Gdx.graphics.getDeltaTime();
        localRange += deltaTime * Config.foregroundMovement;
        if (localRange > 2) {
            localRange = 0;
        }

        // Calcula dificuldade
        if (lastScore != score && score % Config.difficultyDivider == 0) {
            lastScore = score;
            Config.difficulty();
        }

        // Salva maior pontuação
        SharedPreferences.saveHighScore(score);

        if(gameState == Config.GAME_STATE.BEGINNING){//Não iniciado

            if( Gdx.input.justTouched() ){
                gameState = Config.GAME_STATE.RUNNING;
            }
            background.render(0);

        }else {//Iniciado

            fallingRange++;
            if (verticalInitialPosition > 0 || fallingRange < 0) {
                verticalInitialPosition = verticalInitialPosition - fallingRange;
            }

            if(gameState == Config.GAME_STATE.RUNNING){//iniciado

                horizontalObstacleMovementPosition -= deltaTime * Config.obstacleMovement;
                if (Gdx.input.justTouched()) {
                    fallingRange = - Config.jumpRange;
                }
                //Verifica se o cano saiu inteiramente da tela
                if (horizontalObstacleMovementPosition < -Config.OBSTACLE_UP_TX.getWidth()) {
                    horizontalObstacleMovementPosition = deviceWidth;
                    randomSpaceRange = randomNumber.nextInt(Config.OBSTACLE_HEIGHT) - Config.OBSTACLE_HEIGHT / 2;
                    getScore = false;
                }
                //Verifica pontuação
                if(horizontalObstacleMovementPosition < Config.OBSTACLE_WIDTH ){
                    if( !getScore){
                        score++;
                        getScore = true;
                    }
                }
                background.render(deltaTime * Config.backgroundMovement);

            }else{// Game Over
                //Zerar o valores padrões
                if( Gdx.input.justTouched() ){
                    gameState = Config.GAME_STATE.BEGINNING;
                    fallingRange = 0;
                    score = 0;
                    horizontalObstacleMovementPosition = deviceWidth;
                    verticalInitialPosition = deviceHeight / 2;
                    Config.restart();
                }
                background.render(0);
            }
        }

        //Configurar dados de projeção da câmera
        batch.setProjectionMatrix( camera.combined );
        batch.begin();
        batch.draw(Config.OBSTACLE_UP_TX,
                horizontalObstacleMovementPosition,
                deviceHeight / 2 + Config.spaceRange / 2 + randomSpaceRange);
        batch.draw(Config.OBSTACLE_DOWN_TX,
                horizontalObstacleMovementPosition,
                deviceHeight / 2 - Config.OBSTACLE_DOWN_TX.getHeight() - Config.spaceRange / 2 + randomSpaceRange);
        batch.draw(Config.PERSON_TX[(int) localRange],
                Config.PERSON_RATIO,
                verticalInitialPosition);
        GlyphLayout layout = new GlyphLayout(font, String.valueOf(score));
        font.draw(batch, layout,
                (deviceWidth - layout.width)/2,
                deviceHeight - Config.TITLE_RATIO);
        highscore.draw(batch,
                Config.HIGH_SCORE_TEXT.concat(String.valueOf(SharedPreferences.getHighScore())),
                Config.HIGH_SCORE_RATIO,
                Config.HIGH_SCORE_RATIO);
        if( gameState == Config.GAME_STATE.ENDGAME ) {
            message.draw(batch,
                    Config.RESTART_TEXT,
                    deviceWidth / 2 - Config.RESTART_RATIO,
                    deviceHeight / 2 - Config.GAME_OVER_TX.getHeight());
            batch.draw(Config.GAME_OVER_TX,
                    deviceWidth / 2 - Config.GAME_OVER_TX.getWidth() / 2,
                    deviceHeight / 2);
        }
        batch.end();

        personArea.set(Config.PERSON_RATIO + Config.PERSON_TX[0].getWidth() / 2,
                verticalInitialPosition + Config.PERSON_TX[0].getHeight() / 2,
                Config.PERSON_TX[0].getWidth() / 2);

        Rectangle obstacleDown = new Rectangle(
                horizontalObstacleMovementPosition,
                deviceHeight / 2 - Config.OBSTACLE_DOWN_TX.getHeight() - Config.spaceRange / 2 + randomSpaceRange,
                Config.OBSTACLE_DOWN_TX.getWidth(),
                Config.OBSTACLE_DOWN_TX.getHeight()
        );

        Rectangle obstacleUp = new Rectangle(
                horizontalObstacleMovementPosition,
                deviceHeight / 2 + Config.spaceRange / 2 + randomSpaceRange,
                Config.OBSTACLE_UP_TX.getWidth(),
                Config.OBSTACLE_UP_TX.getHeight()
        );

        //Teste de colisão
        if(Intersector.overlaps(personArea, obstacleDown)
                || Intersector.overlaps(personArea, obstacleUp)
                || verticalInitialPosition <= 0
                || verticalInitialPosition >= deviceHeight){
            gameState = Config.GAME_STATE.ENDGAME;
        }

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }
}
